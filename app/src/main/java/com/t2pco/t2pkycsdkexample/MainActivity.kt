package com.t2pco.t2pkycsdkexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.t2pco.t2pekycsdk.v3.T2PKycSdk
import com.t2pco.t2pekycsdk.v3.services.models.T2PKycSdkConfig
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigInteger
import java.security.MessageDigest

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        etUserRef.setText("0800000000")
        etAuthorizationToken.setText("ZXlKamJHbGxiblJEYjJSbElqb2lSRVZGVUZCUFEwdEZWQ0lzSW1Oc2FXVnVkRXhwWWxabGNuTnBiMjRpT2lJeExqQXVNQ0lzSW10bGVVTnZaR1VpT2lKRVVFdFpRekF3TURBeElpd2liV1YwYUc5a0lqb2lVRTlUVkNJc0luUnBiV1Z6ZEdGdGNDSTZJakl3TWpFd05qRTRNVFUwTlRRMElpd2lkRzlyWlc1VWVYQmxJam9pUXlJc0luVnlhU0k2SWlKOTo2ZTU3MzQ3ZmYwYmE0NjQyZThhZjFjN2RlYjcyMjM2NjY4OTI0ODRmMTY5YzQ5ZGM0MmI5ZDU1OWNiMTIwNjE1YjgwZWI4YWI0MmJkNjVmZWFiNjZlNDE4MzRkMGI4ZjhlNmYwYzI0ZTRmODMxYzY0YzdkYTI5ZTA2NDlmMzk0NQ==")
        btStartKyc.setOnClickListener { startSDK() }
    }

    private fun startSDK() {
        val env = when(rdEnvironment.checkedRadioButtonId){
            R.id.rdDev -> "dev"
            R.id.rdTest -> "test"
            else -> "prod"
        }

        val lang = when(rdLanguage.checkedRadioButtonId){
            R.id.rdTH -> "th"
            else -> "en"
        }

        val config = T2PKycSdkConfig().apply {
            kycUserRef = md5(etUserRef.text.toString()) // kycUserRef is refer to user
            kycToken = etAuthorizationToken.text.toString() // kycToken is key for access SDK
            environment = env // For test server, default is production
            language = lang
        }

        T2PKycSdk.initializeSdk(config).startSdk(this){
            if(it.meta.responseCode != 600){
                // Error
                Toast.makeText(
                    applicationContext,
                    "(${it.meta.responseCode})${it.meta.responseMessage}",
                    Toast.LENGTH_SHORT
                ).show()
                return@startSdk
            }

            // Success
            Toast.makeText(
                applicationContext,
                "Success",
                Toast.LENGTH_SHORT
            ).show()

            // You get user information form T2PKycSdkResponse.Data class
            // Log.d("T2PKycSdkResponse.Data", "${it.data}")
            // You can send kycServiceCode and kycSessionCode to your server
            // for check or retrieve user information from API SDK Server
        }
    }

    private fun md5(input:String): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
    }
}
