# T2PKycSDK Example

# How to use T2PKycSDK?

## Example

To run the example project, clone the repo, and run `install` from the Example directory first.


## Requirements

- Minimun Android API level 24 (minSdkVersion 24)


## Installation

Include the repositories in your level project 'build.gradle'
```kotlin
...
allprojects {
    repositories {
        ...
        maven {
            url "https://bitbucket.org/ch_pilarit/t2pkyccamera_gradle/raw/releases"
            credentials {
                username BITBUCKET_USERNAME
                password BITBUCKET_PASSWORD
            }
            authentication{
                basic(BasicAuthentication)
            }
        }
        maven {
            url "https://bitbucket.org/ch_pilarit/ch_lib/raw/releases"
        }
    }
}
..
```

Include dependencies in your level app 'build.gradle'
```kotlin
...
dependencies {
    ...
    implementation 'com.t2pco.t2pekycsdk:t2pekycsdk:1.1.21'
    ...
}
...
```

##ProGuard

If you are using Proguard, You  must include the code below in proguard-rules.pro file.
```kotlin
...
-keep class net.sf.scuba.smartcards.IsoDepCardService {*;}
-keep class org.jmrtd.** { *; }
-keep class net.sf.scuba.** {*;}
-keep class org.bouncycastle.** {*;}
-keep class org.spongycastle.** {*;}
-keep class org.ejbca.** {*;}
...
```

## Usage

In Activity or Fragment
```kotlin
...
val config = T2PKycSdkConfig().apply {
    kycUserRef = "" // kycUserRef is refer to user
    kycToken = "" // kycToken is key for access SDK
    environment = "test" // For test server, default is production server
    fcmToken = "xxx" // fcmToken is token from Firebase Cloud Messaging. Used for KYC by Dip-Chip
}

T2PKycSdk.initializeSdk(config).startSdk(context){ it ->
    if(it.meta.responseCode != 600){
        // Error
        return@startSdk
    }

    // Success
    // You get user information form T2PKycSdkResponse.Data class
    Log.d("T2PKycSdkResponse.Data", "${it.data}")
    // You can send kycServiceCode and kycSessionCode to your server
    // for check or retrieve user information from API SDK Server
}   	
...
```

## Author

Chettha, chettha_pil@t2pco.com